const axios = require("axios");
const _ = require("lodash");

(async () => {
  for (let index = 0; index < 2000; index++) {
    const randomCep = _.sample(["08743040", "04534004", "01407100"]);

    const {
      data: { cep, neighborhood, service },
    } = await axios.get(`https://brasilapi.com.br/api/cep/v1/${randomCep}`);

    console.log(
      `[${index}] - CEP: ${cep} ${neighborhood} - PROVIDER: ${service}`
    );
  }
})();
